﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class character : MonoBehaviour {
    public string chineseCharacter;
    public string[] stroke;
    public SpriteRenderer[] strokeDisplay;
    public AudioClip sound;
    public List<List<int>> strokes = new List<List<int>>();
    public List<List<bool>> drew = new List<List<bool>>();

    // Use this for initialization
    void Start () {
		
		GameObject.Find("btnVol").GetComponent<Button>().onClick.AddListener(delegate {
			GetComponent<AudioSource>().clip = sound; 
			GetComponent<AudioSource>().Play();
		});
		
		if(stroke.Length == 0) {
            Debug.LogError("No stroke");
        } else {
            if(stroke.Length == strokeDisplay.Length) {
                foreach (SpriteRenderer sr in strokeDisplay) {
                    sr.enabled = false;
                }
                foreach (string s in stroke) {
                    List<int> item = new List<int>();
                    List<bool> item2 = new List<bool>();
                    if (s.IndexOf(',') == -1) {
                        item.Add(int.Parse(s));
                        item2.Add(false);
                    } else {
                        foreach (string s2 in s.Split(',')) {
                            item.Add(int.Parse(s2));
                            item2.Add(false);
                        }
                    }
                    strokes.Add(item);
                    drew.Add(item2);
                }
                GameObject.Find("Canvas").GetComponent<draw>().getStrokeNum();
            } else {
                Debug.LogError("Stroke Display Not Equal");
            }
        }
        if(chineseCharacter == null || chineseCharacter == "") {
            Debug.LogError("No Chinese Character");
        }
	}
	

	// Update is called once per frame
	void Update () {
		
	}
}
