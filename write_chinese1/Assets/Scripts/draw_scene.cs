﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class draw_scene : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameObject.Find("chars").GetComponent<SpriteRenderer>().enabled = true;
        GameObject.Find("chars").GetComponent<PolygonCollider2D>().enabled = true;
        foreach(PolygonCollider2D p in GameObject.Find("chars").GetComponentsInChildren<PolygonCollider2D>()) {
            p.enabled = true;
        }
        GameObject.Find("chars").GetComponent<character>().enabled = true;
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
