﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Change_Text : MonoBehaviour
{
    // Start is called before the first frame update

	//int index;
	//private List<string> RadicalList = new List<string>();
	Text mytext;

    void Update()
    {	
		mytext = GameObject.Find("TopText").GetComponent<Text>();
		if (Globals.currentscene==1)
		{

			mytext.text="部 首";
		}
		if (Globals.currentscene==2)
		{

			mytext.text=ShowRadical();
		}
		if (Globals.currentscene>=3)
			mytext.text="  "+ShowWord();
    }
	
    // Update is called once per frame
	public string ShowRadical(){
			foreach (var item in Path.radical){
				if (item.id==Globals.id)
					return item.character;
			}
		return null;
	}
	
	public string ShowWord(){
			foreach (var item in Path.nature){
				if (item.id==Globals.id)
					return item.character;
			}
			foreach (var item in Path.nature){
				if (item.sim_id==Globals.id)
					return item.character;
			}
			return null;
	}
}
