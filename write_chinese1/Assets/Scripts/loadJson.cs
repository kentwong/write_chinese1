using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;

public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    [System.Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}

public static class Path
{

	[System.Serializable]
	public class datainfo
    {
        public string id;
		public string character;
        public int stroke;
		public string path;
		public string radical;
		public string sim_id;
        
    }
	public static datainfo[] radical;
	public static datainfo[] nature;
	public static datainfo[] number;
	public static datainfo[] zodiac;
	public static datainfo[] human;
	public static string path;
	   
}

public class loadJson : MonoBehaviour
{
	void Start(){
		Path.radical=load("Assets/Resources/character/data/radical.json");
		foreach (var item in Path.radical){
			print(item.id);
		}
	}
	


	public static Path.datainfo[] load(string path)
    {
	 StreamReader file = new StreamReader(path);
	 string loadJson = file.ReadToEnd();
     file.Close();
	 print(loadJson);
	// loadJson = "{\r\n    \"Items\": [\r\n        {\r\n            \"playerId\": \"8484239823\",\r\n            \"playerLoc\": \"Powai\",\r\n            \"playerNick\": \"Random Nick\"\r\n        },\r\n        {\r\n            \"playerId\": \"512343283\",\r\n            \"playerLoc\": \"User2\",\r\n            \"playerNick\": \"Rand Nick 2\"\r\n        }\r\n    ]\r\n}";
	 
	return JsonHelper.FromJson<Path.datainfo>(loadJson);
	//print(radical[1].id);
	   
    }
}


