
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class level_control : MonoBehaviour {
	public AudioClip Sound;
	Text mytext;
	void Start () {

	}
	
	
public void btnRadical(){
	
	mytext = GameObject.Find("BoxText").GetComponent<Text>();
	mytext.text=ShowRadical();
	GameObject.Find("Msg_Box").transform.position=new Vector3(3.0f,3.0f,0.5f);
	
}

public void btnStroke(){

	mytext = GameObject.Find("BoxText").GetComponent<Text>();
	mytext.text=ShowStroke().ToString()+"劃";
	GameObject.Find("Msg_Box").transform.position=new Vector3(3.0f,3.0f,0.5f);
	
}

public string ShowRadical(){
	foreach (var item in Path.nature){
		if (item.id==Globals.id||item.sim_id==Globals.id)
			return item.radical;
	}
			
	return null;
}

public static int ShowStroke(){
	foreach (var item in Path.nature){
		if (item.id==Globals.id||item.sim_id==Globals.id)
			return item.stroke;
	}
			
	return 0;
}

public void release(){
	GameObject.Find("Msg_Box").transform.position=new Vector3(-2550.0f,0.0f,1.0f);
}

Sprite img,invisible ;
public void testSroke(){
	invisible=Resources.Load <Sprite>("resource/invisible");
	for (int i=1;i<=ShowStroke();i++){
		GameObject.Find("stroke"+i).GetComponent<SpriteRenderer>().sprite = invisible;
	}
	
}


public void testrestore(){
	print("testrestore");

	string path="character/words/"+Path.nature[0].path+"/";
	for (int i=1;i<=ShowStroke();i++){
		print(path+Globals.id+"_"+(i+1));
		img=Resources.Load <Sprite>(path+Globals.id+"_"+(i+1));
		GameObject.Find("stroke"+i).GetComponent<SpriteRenderer>().sprite = img;
	}
}

}



