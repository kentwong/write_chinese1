﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class draw : MonoBehaviour {
    LineRenderer line;
    private bool isMousePressed;
    public List<Vector3> pointsList;
    private Vector3 mousePos;
    int drawTime = 1;
    private bool abandonLine = false;
    private bool abandonLine2 = false;
    GameObject currentLine = null;
    public AudioClip sndSuccess,sndOops,sndWin,sndClick;
    public static bool textComplete = false;
    
    static int currentStroke = 0;
    static int currentStrokeNum = 2;
    List<int> viaCheckpoint = new List<int>();
	static List<int> actualDrawn = new List<int>();
	static int actualDrawn_leng=0;
    struct myLine {
        public Vector3 StartPoint;
        public Vector3 EndPoint;
    };

    void Start () {
        isMousePressed = false;
        pointsList = new List<Vector3>();
        
        GameObject.Find("btnUndo").GetComponent<Button>().onClick.AddListener(delegate {
            GetComponent<AudioSource>().clip = sndClick; GetComponent<AudioSource>().Play();
            if (currentStroke > 0 && !textComplete) {

				int lastEle=actualDrawn[actualDrawn.Count-1];
                Destroy(GameObject.Find("line" + lastEle));
				actualDrawn.RemoveAt(actualDrawn.Count - 1);
			

                currentStroke--;
                currentStrokeNum = GameObject.Find(Globals.char_name).GetComponent<character>().strokes[currentStroke].Count;
            }
        });
		
		GameObject.Find("btnClear").GetComponent<Button>().onClick.AddListener(delegate {
            GetComponent<AudioSource>().clip = sndClick; GetComponent<AudioSource>().Play();
            if (currentStroke > 0) {
				clearAll();
            }
        });
		
		GameObject.Find("btnNext").GetComponent<Button>().onClick.AddListener(delegate {
            GetComponent<AudioSource>().clip = sndClick; GetComponent<AudioSource>().Play();
            if (currentStroke > 0) {
				clearAll();
            }
        });
		
		GameObject.Find("btnHome").GetComponent<Button>().onClick.AddListener(delegate {
            GetComponent<AudioSource>().clip = sndClick; GetComponent<AudioSource>().Play();
            if (currentStroke > 0) {
				clearAll();
            }
        });
		
    }
	
	public static void clearAll(){

			GameObject[] obj =  GameObject.FindGameObjectsWithTag ("stroke");
			foreach(GameObject ob in obj)
				 GameObject.Destroy(ob);

				
				textComplete=false;
				print("cleared");
				actualDrawn.Clear();
				actualDrawn_leng=0;
                currentStroke=0;
                currentStrokeNum = GameObject.Find(Globals.char_name).GetComponent<character>().strokes[currentStroke].Count;
	}
    public void getStrokeNum () {
        currentStrokeNum = GameObject.Find(Globals.char_name).GetComponent<character>().strokes[currentStroke].Count;
    }
 
    void Update () {
        //Display Current Stroke
        foreach (SpriteRenderer sr in GameObject.Find(Globals.char_name).GetComponent<character>().strokeDisplay) {
            sr.enabled = false;
        }

        //Tackle Draw
        if (!textComplete) {
            GameObject.Find(Globals.char_name).GetComponent<character>().strokeDisplay[currentStroke].enabled = true;
            if (Input.GetMouseButtonDown(0)) {
                mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mousePos.z = -1;
                RaycastHit2D hit = Physics2D.Raycast(new Vector2(mousePos.x, mousePos.y), Vector2.zero, Mathf.Infinity);
                bool startCorrect = false;
                if (hit.collider == null) {
                    startCorrect = true;
                } else {
                    if(hit.collider.name != "border") {
                        startCorrect = true;
                    } else {
                        startCorrect = false;
                    }
                }
                if (startCorrect) {
                    abandonLine = false;
                    isMousePressed = true;
                    viaCheckpoint.Clear();
                    GameObject goLine = new GameObject();
                    goLine.transform.SetParent(GameObject.Find("Camera").transform);
                    goLine.AddComponent<LineRenderer>();
                    goLine.name = "line" + drawTime.ToString();
					print("86:" +goLine.name);
                    goLine.tag = "stroke";
                    currentLine = goLine;
                    drawTime++;
                    line = goLine.GetComponent<LineRenderer>();
                    line.useWorldSpace = false;
                    line.numCornerVertices = 0;
                    line.material = Resources.Load<Material>("material/ink");
                    line.startWidth = 0.1f;
                    line.endWidth = 0.1f;
                    pointsList.RemoveRange(0, pointsList.Count);
                }
            }

            if (isMousePressed) {
                mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mousePos.z = -1;
                if (!pointsList.Contains(mousePos)) {
                    pointsList.Add(mousePos);
                    line.positionCount = pointsList.Count;
                    //line.SetVertexCount(pointsList.Count);
                    line.SetPosition(pointsList.Count - 1, (Vector3)pointsList[pointsList.Count - 1]);
                }
                RaycastHit2D hit = Physics2D.Raycast(new Vector2(mousePos.x, mousePos.y), Vector2.zero, Mathf.Infinity);
                if (hit.collider == null) {
                    print("outBound");
                    abandonLine = true;
                } else {
                    if (hit.collider.name.Substring(0, 5) == "track") {
                        int currentHit = int.Parse(hit.collider.name.Substring(5));

                        if (viaCheckpoint.Count == 0) {
                            viaCheckpoint.Add(currentHit);
                        } else {
                            if (viaCheckpoint[viaCheckpoint.Count - 1] != currentHit) {
                                viaCheckpoint.Add(currentHit);
                            }
                        }
                        if (currentStrokeNum == viaCheckpoint.Count) {
                            //print("OK");
                        } else if (currentStrokeNum > viaCheckpoint.Count) {
                           // print("Over");
                        } else {
                            //print("Less");
                        }
                    }
                    //print(hit.collider.name);
                }
                if (isLineCollide()) {
                    print("outBound2");
                    abandonLine = true;
                }
            }
            if (Input.GetMouseButtonUp(0)) {
                if (abandonLine) {
                    if (!abandonLine2) {
                        Destroy(currentLine); GetComponent<AudioSource>().clip = sndOops; GetComponent<AudioSource>().Play();
                    } else {
                        abandonLine2 = false;
                    }
                } else {
                    bool finishCorrect = true;
                    mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    mousePos.z = -1;
                    RaycastHit2D hit = Physics2D.Raycast(new Vector2(mousePos.x, mousePos.y), Vector2.zero, Mathf.Infinity);
                    if(hit.collider != null) {
                        if(hit.collider.name == "border") {
                            finishCorrect = false;
                        }
                    }
                    if (finishCorrect) {
                        if (currentStrokeNum == viaCheckpoint.Count) {
                            if (hit.collider.name.Substring(0, 5) == "track") {
                                int currentHit = int.Parse(hit.collider.name.Substring(5));
                                int wantHit = GameObject.Find(Globals.char_name).GetComponent<character>().strokes[currentStroke][currentStrokeNum - 1];
                               // print(currentHit + ":" + wantHit);
                                if (currentHit == wantHit) {
                                    //
                                    List<int> checkpointList = GameObject.Find(Globals.char_name).GetComponent<character>().strokes[currentStroke];
                                    bool failed = false;
                                    for (int i = 0; i < checkpointList.Count; i++) {
                                        if (checkpointList[i] != viaCheckpoint[i]) {
                                            failed = true;
                                            break;
                                        }
                                    }
                                    if (!failed) {
										int preDrawn=drawTime-1;
										print("171:"+preDrawn);
										
                                        print("OK");
                                        currentStroke++;
                                        if (currentStroke >= GameObject.Find(Globals.char_name).GetComponent<character>().strokes.Count) {
                                            GetComponent<AudioSource>().clip = sndWin;
                                            GetComponent<AudioSource>().Play();
                                            print("Complete");
                                            textComplete = true;
											print("Current Scene"+Globals.currentscene);
											//Globals.currentscene
											if (Globals.currentscene==3){
												GameObject.Find("Right_3").transform.position=new Vector3(1.9f,-2.5f,0.0f);
												Sprite img;
												string path="resource/progress1";
			
												img=Resources.Load<Sprite>(path);
												GameObject.Find("Flag").GetComponent<Image>().sprite = img;
												Globals.level1=true;
			
											}
												
											if (Globals.currentscene==4){
											
												GameObject.Find("Left_3").transform.position=new Vector3(-1.9f,-2.5f,0.0f);
												GameObject.Find("Right_3").transform.position=new Vector3(1.9f,-2.5f,0.0f);
												Sprite img;
												string path="resource/progress2";
													Globals.level2=true;
												img=Resources.Load<Sprite>(path);
												GameObject.Find("Flag").GetComponent<Image>().sprite = img;
												
											}
                                            //load next word
                                            print("Finish!!");
                                        } else {
                                            GetComponent<AudioSource>().clip = sndSuccess;
                                            GetComponent<AudioSource>().Play();
                                            currentStrokeNum = GameObject.Find(Globals.char_name).GetComponent<character>().strokes[currentStroke].Count;
											actualDrawn.Add(preDrawn);
											actualDrawn_leng++;
                                            foreach (int i in actualDrawn){
												print(i);
											}
										}
                                        print("OK2");
                                    } else {
                                        print("Not OK");
                                        Destroy(currentLine); GetComponent<AudioSource>().clip = sndOops; GetComponent<AudioSource>().Play();
                                    }
                                } else {
                                    print("Not OK3");
                                    Destroy(currentLine); GetComponent<AudioSource>().clip = sndOops; GetComponent<AudioSource>().Play();
                                }
                            } else {
                                print("Not OK2");
                                Destroy(currentLine); GetComponent<AudioSource>().clip = sndOops; GetComponent<AudioSource>().Play();
                            }
                        } else {
                            print("Not Enough Checkpoint");
                            Destroy(currentLine); GetComponent<AudioSource>().clip = sndOops; GetComponent<AudioSource>().Play();
                        }
                    } else {
                        print("Out bounds");
                        Destroy(currentLine); GetComponent<AudioSource>().clip = sndOops; GetComponent<AudioSource>().Play();
                    }
                }
                isMousePressed = false;
            }
        }
    }
    private bool isLineCollide () {
        if (pointsList.Count < 2)
            return false;
        int TotalLines = pointsList.Count - 1;
        myLine[] lines = new myLine[TotalLines];
        if (TotalLines > 1) {
            for (int i = 0; i < TotalLines; i++) {
                lines[i].StartPoint = (Vector3)pointsList[i];
                lines[i].EndPoint = (Vector3)pointsList[i + 1];
            }
        }
        for (int i = 0; i < TotalLines - 1; i++) {
            myLine currentLine;
            currentLine.StartPoint = (Vector3)pointsList[pointsList.Count - 2];
            currentLine.EndPoint = (Vector3)pointsList[pointsList.Count - 1];
            if (isLinesIntersect(lines[i], currentLine))
                return true;
        }
        return false;
    }
    //    -----------------------------------    
    //    Following method checks whether given two points are same or not
    //    -----------------------------------    
    private bool checkPoints (Vector3 pointA, Vector3 pointB) {
        return (pointA.x == pointB.x && pointA.y == pointB.y);
    }
    //    -----------------------------------    
    //    Following method checks whether given two line intersect or not
    //    -----------------------------------    
    private bool isLinesIntersect (myLine L1, myLine L2) {
        if (checkPoints(L1.StartPoint, L2.StartPoint) ||
            checkPoints(L1.StartPoint, L2.EndPoint) ||
            checkPoints(L1.EndPoint, L2.StartPoint) ||
            checkPoints(L1.EndPoint, L2.EndPoint))
            return false;

        return ((Mathf.Max(L1.StartPoint.x, L1.EndPoint.x) >= Mathf.Min(L2.StartPoint.x, L2.EndPoint.x)) &&
            (Mathf.Max(L2.StartPoint.x, L2.EndPoint.x) >= Mathf.Min(L1.StartPoint.x, L1.EndPoint.x)) &&
            (Mathf.Max(L1.StartPoint.y, L1.EndPoint.y) >= Mathf.Min(L2.StartPoint.y, L2.EndPoint.y)) &&
            (Mathf.Max(L2.StartPoint.y, L2.EndPoint.y) >= Mathf.Min(L1.StartPoint.y, L1.EndPoint.y))
               );
    }
}
