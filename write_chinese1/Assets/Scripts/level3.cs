﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class level3 : MonoBehaviour {
    LineRenderer line;
    private bool isMousePressed;
    public List<Vector3> pointsList;
    private Vector3 mousePos;
    GameObject currentLine = null;
    public AudioClip sndSuccess,sndOops,sndWin,sndClick;
    bool textComplete = false;
	static int drawTime=0;
	static int lastDel=0;
	static List<int> actualDrawn = new List<int>();
	private bool abandonLine=false;

    struct myLine {
        public Vector3 StartPoint;
        public Vector3 EndPoint;
    };

    void Start () {
		textComplete=false;
		GetComponent<AudioSource>().clip = sndWin; 
		print("27start");
        isMousePressed = false;
        pointsList = new List<Vector3>();
		GameObject.Find("btnClear").GetComponent<Button>().onClick.AddListener(() => clearAll());
		
		GameObject.Find("btnUndo").GetComponent<Button>().onClick.AddListener(() => clearPrev());

		GameObject.Find("level3ok").GetComponent<Button>().onClick.AddListener(() => win());
            
				

       
    }
	
	public static void clearAll(){
	
		for (int i=lastDel;i<drawTime;i++){
			
			Destroy(GameObject.Find("line"+i.ToString()));
			lastDel=i;
			actualDrawn.Clear();

		}

	}
	
	public void clearPrev(){
		//GetComponent<AudioSource>().clip = sndClick; GetComponent<AudioSource>().Play();
		int todel=actualDrawn[actualDrawn.Count - 1];
		print("58"+todel);
		Destroy(GameObject.Find("line"+todel.ToString()));
		actualDrawn.RemoveAt(actualDrawn.Count - 1);

	}
	
	public void win(){
		Globals.test=true;
		GetComponent<AudioSource>().clip = sndWin; 
		GetComponent<AudioSource>().Play();
		print("win");
		
		Globals.level3=true;
		Sprite img;
		string path="resource/progress3";
		Globals.level2=true;
		img=Resources.Load<Sprite>(path);
		GameObject.Find("Flag").GetComponent<Image>().sprite = img;
		
	}
	
	
    void Update () {

        //Tackle Draw
        if (!textComplete) {
 
            if (Input.GetMouseButtonDown(0)) {
                mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mousePos.z = -1;
                isMousePressed = false;
				RaycastHit2D hit = Physics2D.Raycast(new Vector2(mousePos.x, mousePos.y), Vector2.zero, Mathf.Infinity);
				bool startCorrect = false;
                if (hit.collider == null) {
                    startCorrect = true;
                } else {
                    if(hit.collider.name != "border") {
                        startCorrect = true;
                    } else {
						//GetComponent<AudioSource>().clip = sndOops; GetComponent<AudioSource>().Play();
                        startCorrect = false;
                    }
                }
				if (startCorrect){
					  isMousePressed = true;
                GameObject goLine = new GameObject();
                goLine.transform.SetParent(GameObject.Find("Camera").transform);
                goLine.AddComponent<LineRenderer>();
                goLine.name = "line" + drawTime.ToString();
				 goLine.tag = "stroke";
                currentLine = goLine;

                drawTime++;
                line = goLine.GetComponent<LineRenderer>();
                line.useWorldSpace = false;
                line.numCornerVertices = 0;
                line.material = Resources.Load<Material>("material/ink");
                line.startWidth = 0.1f;
                line.endWidth = 0.1f;
                pointsList.RemoveRange(0, pointsList.Count);
                }
            }

            if (isMousePressed) {
                mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                mousePos.z = -1;
				
                if (!pointsList.Contains(mousePos)) {
                    pointsList.Add(mousePos);
                    line.positionCount = pointsList.Count;
                    line.SetPosition(pointsList.Count - 1, (Vector3)pointsList[pointsList.Count - 1]);
                }
				

				
            }
			
            if (Input.GetMouseButtonUp(0)) {

                bool finishCorrect = true;
				RaycastHit2D hit = Physics2D.Raycast(new Vector2(mousePos.x, mousePos.y), Vector2.zero, Mathf.Infinity);
                    if(hit.collider != null) {
                        if(hit.collider.name == "border") {
							//GetComponent<AudioSource>().clip = sndOops; GetComponent<AudioSource>().Play();
                            finishCorrect = false;
							if (Globals.test==false)
								Destroy(currentLine);
                        }
                
				}
				if (finishCorrect){
					mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
					mousePos.z = -1;
					actualDrawn.Add(drawTime-1);
					
					//GetComponent<AudioSource>().clip = sndSuccess;
					//GetComponent<AudioSource>().Play();
					
				}

                isMousePressed = false;
            }
        }
    }

}
